# Phishing site hosts generator
A host file generated from updated phishing site feeds.

This isn't for everyone, it's aimed at the vulnerable (Your parents, grandparents, disabled, etc) who might fall victim to phishing emails.

The hosts file is updated daily and will be happy to increase that if anyone raises concern. While all of the assets are here, it's unlikely you'll need all of it, just copy and paste the [raw hosts file](https://raw.githubusercontent.com/kurobeats/phishing_hosts/master/hosts) via automated (or manual if you're a masochist) means.

Include the hosts file in whatever hosts file updating script/program you use (I personally recommend [Steven Black's](https://github.com/StevenBlack/hosts) awesome script) to allow for seemless updates...or make one and send me a pull request!
